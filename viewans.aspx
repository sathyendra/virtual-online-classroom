﻿<%@ Page Title="" Language="VB" MasterPageFile="~/studentmaster.master" AutoEventWireup="false" CodeFile="viewans.aspx.vb" Inherits="viewans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
    <br />
</p>
<p>
    <asp:GridView ID="GridView1" runat="server" BackColor="White" 
        BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
        GridLines="Vertical" Height="208px" Width="760px" 
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <AlternatingRowStyle BackColor="#DCDCDC" />
        <Columns>
            <asp:BoundField DataField="topic" HeaderText="TOPIC" SortExpression="topic" />
            <asp:BoundField DataField="doubt" HeaderText="DOUBT" SortExpression="doubt" />
            <asp:BoundField DataField="answer" HeaderText="ANSWER" 
                SortExpression="answer" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#0000A9" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:VirtualClassDBConnectionString8 %>" 
        SelectCommand="SELECT [topic], [doubt], [fname], [answer] FROM [postdoubts] WHERE ([sid] = @sid)">
        <SelectParameters>
            <asp:SessionParameter Name="sid" SessionField="sid" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</p>
</asp:Content>

