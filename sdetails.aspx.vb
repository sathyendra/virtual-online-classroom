﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class sdetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt As New DataTable()
        Dim con As SqlConnection = New SqlConnection("data source=JIVANRAO-PC\SQLEXPRESS;initial catalog=VirtualClassDB;integrated Security=true")
        Dim strQuery As String = "select sname,sid,branch,emailid,phno,dob,gender,password,question,answer,photo from studreg"
        Dim cmd As New SqlCommand(strQuery)
        Dim sda As New SqlDataAdapter()
        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            GridView1.DataSource = dt
            GridView1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Sub
End Class
