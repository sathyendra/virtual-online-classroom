﻿<%@ Page Title="" Language="VB" MasterPageFile="~/adminmaster.master" AutoEventWireup="false" CodeFile="fdetails.aspx.vb" Inherits="fdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <br />
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical">
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <Columns>
                <asp:BoundField DataField="fname" HeaderText="Faculty" />
                <asp:BoundField DataField="fid" HeaderText="Roll No" />
                <asp:BoundField DataField="branch" HeaderText="Branch" />
                <asp:BoundField DataField="emailid" HeaderText="EmailId" />
                <asp:BoundField DataField="phno" HeaderText="Phone No" />
                <asp:BoundField DataField="dob" HeaderText="DOB" />
                <asp:BoundField DataField="gender" HeaderText="Gender" />
                <asp:BoundField DataField="password" HeaderText="Password" />
                <asp:BoundField DataField="question" HeaderText="Question" />
                <asp:BoundField DataField="answer" HeaderText="Answer" />
                <asp:BoundField DataField="designation" HeaderText="Designation" />
                <asp:ImageField DataImageUrlField="photo" ControlStyle-Width="100" ControlStyle-Height = "100" HeaderText = "Image"/>
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
        &nbsp;</p>
</asp:Content>

