﻿<%@ Page Title="" Language="VB" MasterPageFile="~/adminmaster.master" AutoEventWireup="false" CodeFile="viewfdetails.aspx.vb" Inherits="viewfdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <br />
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" 
            CellPadding="3" DataKeyNames="fid" DataSourceID="SqlDataSource1" 
            GridLines="Vertical">
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="fname" HeaderText="fname" SortExpression="fname" />
                <asp:BoundField DataField="fid" HeaderText="fid" ReadOnly="True" 
                    SortExpression="fid" />
                <asp:BoundField DataField="branch" HeaderText="branch" 
                    SortExpression="branch" />
                <asp:BoundField DataField="emailid" HeaderText="emailid" 
                    SortExpression="emailid" />
                <asp:BoundField DataField="phno" HeaderText="phno" SortExpression="phno" />
                <asp:BoundField DataField="dob" HeaderText="dob" SortExpression="dob" />
                <asp:BoundField DataField="gender" HeaderText="gender" 
                    SortExpression="gender" />
                <asp:BoundField DataField="password" HeaderText="password" 
                    SortExpression="password" />
                <asp:BoundField DataField="question" HeaderText="question" 
                    SortExpression="question" />
                <asp:BoundField DataField="answer" HeaderText="answer" 
                    SortExpression="answer" />
                <asp:BoundField DataField="designation" HeaderText="designation" 
                    SortExpression="designation" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:VirtualClassDBConnectionString6 %>" 
            DeleteCommand="DELETE FROM [staffreg] WHERE [fid] = @fid" 
            InsertCommand="INSERT INTO [staffreg] ([fname], [fid], [branch], [emailid], [phno], [dob], [gender], [password], [question], [answer], [designation]) VALUES (@fname, @fid, @branch, @emailid, @phno, @dob, @gender, @password, @question, @answer, @designation)" 
            SelectCommand="SELECT [fname], [fid], [branch], [emailid], [phno], [dob], [gender], [password], [question], [answer], [designation] FROM [staffreg]" 
            UpdateCommand="UPDATE [staffreg] SET [fname] = @fname, [branch] = @branch, [emailid] = @emailid, [phno] = @phno, [dob] = @dob, [gender] = @gender, [password] = @password, [question] = @question, [answer] = @answer, [designation] = @designation WHERE [fid] = @fid">
            <DeleteParameters>
                <asp:Parameter Name="fid" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="fname" Type="String" />
                <asp:Parameter Name="fid" Type="String" />
                <asp:Parameter Name="branch" Type="String" />
                <asp:Parameter Name="emailid" Type="String" />
                <asp:Parameter Name="phno" Type="String" />
                <asp:Parameter Name="dob" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="password" Type="String" />
                <asp:Parameter Name="question" Type="String" />
                <asp:Parameter Name="answer" Type="String" />
                <asp:Parameter Name="designation" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="fname" Type="String" />
                <asp:Parameter Name="branch" Type="String" />
                <asp:Parameter Name="emailid" Type="String" />
                <asp:Parameter Name="phno" Type="String" />
                <asp:Parameter Name="dob" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="password" Type="String" />
                <asp:Parameter Name="question" Type="String" />
                <asp:Parameter Name="answer" Type="String" />
                <asp:Parameter Name="designation" Type="String" />
                <asp:Parameter Name="fid" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>

