﻿<%@ Page Title="" Language="VB" MasterPageFile="~/studentmaster.master" AutoEventWireup="false" CodeFile="viewschedule.aspx.vb" Inherits="viewschedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <asp:GridView ID="GridView1" runat="server" BackColor="White" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical" Height="186px" Width="582px" 
            AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <Columns>
                <asp:BoundField DataField="FACULTY" HeaderText="FACULTY" 
                    SortExpression="FACULTY" />
                <asp:BoundField DataField="TOPIC" HeaderText="TOPIC" SortExpression="TOPIC" />
                <asp:BoundField DataField="DATE" HeaderText="DATE" SortExpression="DATE" />
                <asp:BoundField DataField="TIME" HeaderText="TIME" SortExpression="TIME" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:VirtualClassDBConnectionString2 %>" 
            SelectCommand="select fname as FACULTY,topic as TOPIC,date as DATE,time as TIME from schedule">
        </asp:SqlDataSource>
        <br />
        <asp:LinkButton ID="LinkButton1" runat="server">Go TO CLASS</asp:LinkButton>
    </p>
</asp:Content>

