﻿<%@ Page Title="" Language="VB" MasterPageFile="~/staffmaster.master" AutoEventWireup="false" CodeFile="editschedule.aspx.vb" Inherits="editschedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <br />
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource1" DataKeyNames="sid" BackColor="White" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical" Width="542px">
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="sid" HeaderText="SNO" ReadOnly="True" 
                    SortExpression="sid" />
                <asp:BoundField DataField="topic" HeaderText="TOPIC" SortExpression="topic" />
                <asp:BoundField DataField="date" HeaderText="DATE" SortExpression="date" />
                <asp:BoundField DataField="time" HeaderText="TIME" SortExpression="time" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:VirtualClassDBConnectionString %>" 
            
            SelectCommand="SELECT [topic], [date], [time], [sid] FROM [schedule] WHERE ([fid] = @fid)" 
            DeleteCommand="DELETE FROM [schedule] WHERE [sid] = @sid" 
            InsertCommand="INSERT INTO [schedule] ([topic], [date], [time], [sid]) VALUES (@topic, @date, @time, @sid)" 
            UpdateCommand="UPDATE [schedule] SET [topic] = @topic, [date] = @date, [time] = @time WHERE [sid] = @sid">
            <DeleteParameters>
                <asp:Parameter Name="sid" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="topic" Type="String" />
                <asp:Parameter Name="date" Type="String" />
                <asp:Parameter Name="time" Type="String" />
                <asp:Parameter Name="sid" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:SessionParameter Name="fid" SessionField="fid" Type="String" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="topic" Type="String" />
                <asp:Parameter Name="date" Type="String" />
                <asp:Parameter Name="time" Type="String" />
                <asp:Parameter Name="sid" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>

