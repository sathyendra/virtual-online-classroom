﻿Imports System.Data.SqlClient
Partial Class downloadmaterial
    Inherits System.Web.UI.Page

  
    Private Sub BindGrid()
        Dim con As SqlConnection = New SqlConnection("data source=JIVANRAO-PC\SQLEXPRESS;initial catalog=VirtualClassDB;integrated Security=true")
      
        Dim cmd As SqlCommand = New SqlCommand()
        cmd.CommandText = "select id,name,fid,fname,datepost,title,description,branch from material where branch='" & Session("sbranch") & "'"
        cmd.Connection = con
        con.Open()
        GridView1.DataSource = cmd.ExecuteReader()
        GridView1.DataBind()
        con.Close()
            
    End Sub

    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)

        Dim id As Integer = Integer.Parse(DirectCast(sender, LinkButton).CommandArgument)
        Dim bytes() As Byte
        Dim fileName, contentType As String
        Dim con As SqlConnection = New SqlConnection("data source=JIVANRAO-PC\SQLEXPRESS;initial catalog=VirtualClassDB;integrated Security=true")
        Dim cmd As SqlCommand = New SqlCommand()
        'cmd.CommandText = "select Name, Data, ContentType from tblFiles where Id=@Id";
        cmd.Connection = con
        cmd.CommandText = "select A.fid,B.fname,A.title,A.description,A.branch,A.datepost,A.name,A.contenttype,A.data from material A, staffreg B where id=@id and A.fid=B.fid and A.branch='" & Session("sbranch") & "'"
        cmd.Parameters.AddWithValue("@id", id)
        con.Open()
        Dim sdr As SqlDataReader = cmd.ExecuteReader()

        If sdr.Read() Then
            bytes = sdr("data")
            contentType = sdr("contenttype").ToString()
            fileName = sdr("name").ToString()
        End If
        con.Close()


        Response.Clear()
        Response.Buffer = True
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = contentType
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BindGrid()
        End If
    End Sub
End Class
