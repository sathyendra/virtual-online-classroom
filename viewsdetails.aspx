﻿<%@ Page Title="" Language="VB" MasterPageFile="~/adminmaster.master" AutoEventWireup="false" CodeFile="viewsdetails.aspx.vb" Inherits="viewsdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <br />
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" BackColor="White" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical" AutoGenerateColumns="False" DataKeyNames="sid" 
            DataSourceID="SqlDataSource1">
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="sname" HeaderText="sname" SortExpression="sname" />
                <asp:BoundField DataField="sid" HeaderText="sid" ReadOnly="True" 
                    SortExpression="sid" />
                <asp:BoundField DataField="branch" HeaderText="branch" 
                    SortExpression="branch" />
                <asp:BoundField DataField="emailid" HeaderText="emailid" 
                    SortExpression="emailid" />
                <asp:BoundField DataField="phno" HeaderText="phno" SortExpression="phno" />
                <asp:BoundField DataField="dob" HeaderText="dob" SortExpression="dob" />
                <asp:BoundField DataField="gender" HeaderText="gender" 
                    SortExpression="gender" />
                <asp:BoundField DataField="password" HeaderText="password" 
                    SortExpression="password" />
                <asp:BoundField DataField="question" HeaderText="question" 
                    SortExpression="question" />
                <asp:BoundField DataField="answer" HeaderText="answer" 
                    SortExpression="answer" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:VirtualClassDBConnectionString7 %>" 
            DeleteCommand="DELETE FROM [studreg] WHERE [sid] = @sid" 
            InsertCommand="INSERT INTO [studreg] ([sname], [sid], [branch], [emailid], [phno], [dob], [gender], [password], [question], [answer]) VALUES (@sname, @sid, @branch, @emailid, @phno, @dob, @gender, @password, @question, @answer)" 
            SelectCommand="SELECT [sname], [sid], [branch], [emailid], [phno], [dob], [gender], [password], [question], [answer] FROM [studreg]" 
            UpdateCommand="UPDATE [studreg] SET [sname] = @sname, [branch] = @branch, [emailid] = @emailid, [phno] = @phno, [dob] = @dob, [gender] = @gender, [password] = @password, [question] = @question, [answer] = @answer WHERE [sid] = @sid">
            <DeleteParameters>
                <asp:Parameter Name="sid" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="sname" Type="String" />
                <asp:Parameter Name="sid" Type="String" />
                <asp:Parameter Name="branch" Type="String" />
                <asp:Parameter Name="emailid" Type="String" />
                <asp:Parameter Name="phno" Type="String" />
                <asp:Parameter Name="dob" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="password" Type="String" />
                <asp:Parameter Name="question" Type="String" />
                <asp:Parameter Name="answer" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="sname" Type="String" />
                <asp:Parameter Name="branch" Type="String" />
                <asp:Parameter Name="emailid" Type="String" />
                <asp:Parameter Name="phno" Type="String" />
                <asp:Parameter Name="dob" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="password" Type="String" />
                <asp:Parameter Name="question" Type="String" />
                <asp:Parameter Name="answer" Type="String" />
                <asp:Parameter Name="sid" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>

